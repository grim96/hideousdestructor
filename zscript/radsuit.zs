//-------------------------------------------------
// Environment/Radiation Suit
//-------------------------------------------------
class WornRadsuit:HDDamageHandler{
	default{
		+nointeraction;+noblockmap;
		inventory.maxamount 1;inventory.amount 1;
		HDDamageHandler.priority 1000;
		HDPickup.wornlayer STRIP_RADSUIT;
	}
	states{spawn:TNT1 A 0;stop;}
	override inventory createtossable(int amt){
		let rrr=owner.findinventory("PortableRadsuit");
		if(rrr)owner.useinventory(rrr);else destroy();
		return null;
	}
	override void attachtoowner(actor owner){
		if(!owner.countinv("PortableRadsuit"))owner.A_GiveInventory("PortableRadsuit");
		super.attachtoowner(owner);
	}
	override void DetachFromOwner(){
		owner.A_TakeInventory("PortableRadsuit",1);
		HDArmour.ArmourChangeEffect(owner);
		super.DetachFromOwner();
	}
	override void DoEffect(){
		if(stamina>0)stamina--;
	}


	//called from HDPlayerPawn and HDMobBase's DamageMobj
	override int,name,int,int,int,int,int HandleDamage(
		int damage,
		name mod,
		int flags,
		actor inflictor,
		actor source,
		int towound,
		int toburn,
		int tostun,
		int tobreak
	){
		let victim=owner;
		if(
			(flags&DMG_NO_ARMOR)
			||mod=="maxhpdrain"
			||mod=="internal"
			||mod=="jointlock"
			||mod=="bleedout"
			||mod=="invisiblebleedout"
			||!victim
		)return damage,mod,flags,towound,toburn,tostun,tobreak;


		if(
			mod!="bashing"
			&&mod!="falling"
		)stamina+=random(1,damage);


		bool breached=false;

		if(mod=="slime"){
			victim.A_GiveInventory("Heat",int(damage*frandom(2.3,2.7)));
			if(
				damage>10
				&&stamina>2100
			){
				breached=true;
			}else if(damage>random(10,50)){
				damage=1;
			}else damage=0;
		}else if(
			mod=="hot"
			||mod=="cold"
		){
			if(damage<random(0,7))damage=0;
			else{
				int olddamage=damage>>1;
				damage=olddamage>>2;
				if(!damage&&random(0,olddamage))damage=1;
				if(stamina>2100)breached=true;
			}
		}else if(mod=="electrical"){
			if(damage>random(60,200))breached=true;
			int olddamage=damage>>2;
			damage=olddamage>>3;
			if(!damage&&random(0,olddamage))damage=1;
		}else if(mod=="slashing"){
			if(damage>random(5,30)){
				A_StartSound("radsuit/rip",CHAN_BODY,CHANF_OVERLAP);
				breached=true;
			}
		}else if(
			mod=="teeth"
			||mod=="claws"
			||mod=="natural"
		){
			if(random(1,damage)>10){
				A_StartSound("radsuit/rip",CHAN_BODY,CHANF_OVERLAP);
				breached=true;
				damage-=5;
			}
		}else{
			//any other damage not taken care of above
			if(towound>random(4,20))breached=true;
		}

		if(breached)destroyradsuit();

		return damage,mod,flags,towound,toburn,tostun,tobreak;
	}
	void DestroyRadsuit(){	
		destroy();
		if(owner){
			owner.A_TakeInventory("PowerIronFeet");
			owner.A_StartSound("radsuit/burst",CHAN_BODY,CHANF_OVERLAP);
		}
	}

	//called from HDBulletActor's OnHitActor
	override double,double OnBulletImpact(
		HDBulletActor bullet,
		double pen,
		double penshell,
		double hitangle,
		double deemedwidth,
		vector3 hitpos,
		vector3 vu,
		bool hitactoristall
	){
		if(pen>frandom(1,4))destroyradsuit();
		penshell+=1;
		return pen,penshell;
	}

}
class PortableRadsuit:HDPickup replaces RadSuit{
	default{
		//$Category "Gear/Hideous Destructor/Supplies"
		//$Title "Environment Suit"
		//$Sprite "SUITA0"

		inventory.pickupmessage "Environmental shielding suit.";
		inventory.pickupsound "weapons/pocket";
		inventory.icon "SUITB0";
		hdpickup.bulk ENC_RADSUIT;
		tag "environment suit";
		hdpickup.refid HDLD_RADSUIT;
	}
	override void DetachFromOwner(){
		owner.A_TakeInventory("PortableRadsuit");
		owner.A_TakeInventory("WornRadsuit");
		target=owner;
		super.DetachFromOwner();
	}
	override inventory CreateTossable(){
		if(
			amount<2
			&&owner.findinventory("WornRadsuit")
		){
			owner.UseInventory(self);
			return null;
		}
		return super.CreateTossable();
	}
	override void actualpickup(actor user){
		super.actualpickup(user);
		//put on the radsuit right away
		wornlayer=STRIP_RADSUIT;
		if(
			!user.findinventory("WornRadsuit")
			&&user.player&&user.player.cmd.buttons&BT_USE
			&&HDPlayerPawn.CheckStrip(user,self,false)
		){
			inventory slf=user.findinventory(getclass());
			HDF.TransferFire(slf,user);
			if(slf)user.UseInventory(slf);
		}
		wornlayer=0;
	}
	override void DoEffect(){
		bfitsinbackpack=(amount!=1||!owner||!owner.findinventory("WornRadsuit"));
		super.doeffect();
	}
	states{
	spawn:
		SUIT A 1;
		SUIT A -1{
			if(!target)return;
			HDF.TransferFire(target,self);
		}
	use:
		TNT1 A 0{
			let owrs=wornradsuit(findinventory("wornradsuit"));
			if(owrs){
				if(!HDPlayerPawn.CheckStrip(self,owrs))return;
			}else{
				invoker.wornlayer=STRIP_RADSUIT+1;
				if(!HDPlayerPawn.CheckStrip(self,invoker)){
					invoker.wornlayer=0;
					return;
				}
				invoker.wornlayer=0;
			}

			HDArmour.ArmourChangeEffect(self);
			let onr=HDPlayerPawn(self);
			if(onr)onr.stunned+=60;
			if(!countinv("WornRadsuit")){
				int fff=HDF.TransferFire(self,self);
				if(fff){
					if(random(1,fff)>30){
						A_StartSound("misc/fwoosh",CHAN_AUTO);
						A_TakeInventory("PortableRadsuit",1);
						return;
					}else{
						HDF.TransferFire(self,null);
						if(onr){
							onr.fatigue+=fff;
							onr.stunned+=fff;
						}
					}
				}
				A_GiveInventory("WornRadsuit");
			}else{
				actor a;int b;
				inventory wrs=findinventory("wornradsuit");
				[b,a]=A_SpawnItemEx("PortableRadsuit",0,0,height/2,2,0,4);
				if(a && wrs){
					//transfer sticky fire
					if(wrs.stamina){
						let aa=HDActor(a);
						if(aa)aa.A_Immolate(a,self,wrs.stamina);
					}
					//transfer heat
					let hhh=heat(findinventory("heat"));
					if(hhh){
						double realamount=hhh.realamount;
						double intosuit=clamp(realamount*0.9,0,min(200,realamount));
						let hhh2=heat(a.GiveInventoryType("heat"));
						if(hhh2){
							hhh2.realamount+=intosuit;
							hhh.realamount=max(0,hhh.realamount-intosuit);
						}
					}
				}
				A_TakeInventory("WornRadsuit");
			}
		}fail;
	}
}
