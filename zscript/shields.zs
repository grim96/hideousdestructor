// ------------------------------------------------------------
// Nice movement your objects have there.
// Shame if they got........ damaged.
// ------------------------------------------------------------

class HDMagicShield:HDDamageHandler{
	default{
		+inventory.untossable;+nointeraction;+noblockmap;inventory.amount 1;

		+quicktoretaliate  //if enabled, regenerate up to maxamount
		-standstill  //if enabled, do not deplete if over maxamount
		+inventory.keepdepleted;
		inventory.maxamount 1000;

		HDDamageHandler.priority 10000;
	}
	override void AttachToOwner(actor other){
		super.AttachToOwner(other);
		if(hdmobbase(other)){
			int mmm=hdmobbase(other).maxshields;
			if(mmm>0){
				maxamount=mmm;
				amount=mmm;
			}
		}
	}
	static void Deplete(
		actor owner,
		int amount,
		HDMagicShield shields=null,
		bool destroydepleted=false
	){
		if(!shields)shields=HDMagicShield(owner.findinventory("HDMagicShield"));
		if(shields){
			shields.amount-=amount;
			if(shields.amount<1){
				int downto=-(shields.maxamount>>4);
				shields.amount=downto;
				if(hd_debug)console.printf(owner.getclassname().." shield broke to "..downto.."!");
				owner.A_StartSound("misc/mobshieldx", CHAN_BODY, CHANF_OVERLAP, 0.75);
				double oradius=owner.radius;
				double oheight=owner.height;
				vector3 ovel=owner.vel;
				for(int i=0;i<10;i++){
					vector3 rpos=owner.pos+(
						frandom(-oradius,oradius),
						frandom(-oradius,oradius),
						frandom(0,oheight)
					);
					actor spk=actor.spawn("ShieldSpark",rpos,ALLOW_REPLACE);
					spk.vel=(frandom(-2,2),frandom(-2,2),frandom(-2,2))+ovel;
				}
				if(destroydepleted)shields.destroy();
			}
		}
	}
	override void DoEffect(){
		if(owner.bcorpse||owner.health<1)return;

		if(maxamount<1&&amount<1){
			destroy();
			return;
		}

		//replenish shields and handle breaking/unbreaking
		if(
			!bstandstill
			&&amount>maxamount
		)amount--;
		else if(
			bquicktoretaliate
			&&amount<maxamount
		)amount++;

		if(
			bquicktoretaliate
			&&amount==1
			&&maxamount>1
		){
			if(hd_debug)console.printf(owner.getclassname().." shield restored!");
			owner.A_StartSound("misc/mobshieldf", CHAN_BODY, CHANF_OVERLAP, 0.75);
			double oradius=owner.radius;
			double oheight=owner.height;
			for(int i=0;i<10;i++){
				vector3 rpos=owner.pos+(
					frandom(-oradius,oradius),
					frandom(-oradius,oradius),
					frandom(0,oheight)
				);
				actor spk=actor.spawn("ShieldSpark",rpos,ALLOW_REPLACE);
				vector3 sv = spk.Vec3To(owner);
				sv.z += height/2;
				spk.vel=(sv*(1./50));
			}
		}
	}

	//called from HDPlayerPawn and HDMobBase's DamageMobj
	override int,name,int,int,int,int,int HandleDamage(
		int damage,
		name mod,
		int flags,
		actor inflictor,
		actor source,
		int towound,
		int toburn,
		int tostun,
		int tobreak
	){
		actor victim=owner;
		if(
			!victim
			||(flags&(DMG_NO_FACTOR|DMG_FORCED))
			||amount<1
			||!inflictor
			||(inflictor==victim)
			||(source==victim)
			||(inflictor is "HDBulletActor")
			||mod=="bleedout"
			||mod=="hot"
			||mod=="cold"
			||mod=="maxhpdrain"
			||mod=="internal"
			||mod=="holy"
			||mod=="jointlock"
			||mod=="staples"
		)return damage,mod,flags,towound,toburn,tostun,tobreak;

		int blocked=min(amount>>1,damage,512);
		damage-=blocked;
		bool supereffective=(
			mod=="BFGBallAttack"
			||mod=="electrical"
			||mod=="balefire"
		);

		HDMagicShield.Deplete(victim,max(supereffective?(blocked<<2):blocked,1),self);


		if(hd_debug)console.printf("BLOCKED (not bullet)  "..blocked.."    OF  "..damage+blocked..",   "..amount.." REMAIN");


		//spawn shield debris
		vector3 sparkpos;
		if(
			inflictor
			&&inflictor!=source
		)sparkpos=inflictor.pos;
		else if(
			source
		)sparkpos=(
			victim.pos.xy+victim.radius*(source.pos.xy-victim.pos.xy).unit()
			,victim.pos.z+min(victim.height,source.height*0.6)
		);
		else sparkpos=(victim.pos.xy,victim.pos.z+victim.height*0.6);

		int shrd=max(1,blocked>>6);
		for(int i=0;i<shrd;i++){
			actor aaa=victim.spawn("ShieldSpark",sparkpos,ALLOW_REPLACE);
			aaa.vel=(frandom(-3,3),frandom(-3,3),frandom(-3,3));
		}

		//chance to flinch
		if(damage<1){
			if(
				!(flags&DMG_NO_PAIN)
				&&blocked>(victim.spawnhealth()>>3)
				&&random(0,255)<victim.painchance
			)hdmobbase.forcepain(victim);
		}

		return damage,mod,flags,towound,toburn,tostun,tobreak;
	}

	//called from HDBulletActor's OnHitActor
	override double,double OnBulletImpact(
		HDBulletActor bullet,
		double pen,
		double penshell,
		double hitangle,
		double deemedwidth,
		vector3 hitpos,
		vector3 vu,
		bool hitactoristall
	){
		actor victim=owner;
		if(
			!victim
			||!bullet
			||(bullet.target==victim)
			||amount<1
		)return pen,penshell;


		int bulletpower=int(pen*bullet.mass*0.1);
		int depleteshield=min(bulletpower,amount);


		if(hd_debug)console.printf("BLOCKED  "..depleteshield.."    OF  "..bulletpower..",   "..amount-bulletpower.." REMAIN");


		if(depleteshield<=0)return pen,penshell;

		HDMagicShield.Deplete(victim,depleteshield,self);
		spawn("ShieldNeverBlood",bullet.pos,ALLOW_REPLACE);


		victim.vel+=(
			((victim.pos.xy,victim.pos.z+victim.height*0.5)-bullet.pos).unit()
			*depleteshield
			/victim.mass
		);
		victim.angle+=deltaangle(victim.angle,victim.angleto(bullet))*frandom(-0.005,0.03);
		victim.pitch+=frandom(-1.,1.);

		penshell+=min(pen,amount,maxamount>>3);
		return pen,penshell;
	}
}


//standalone puff that replaces blood
class ShieldSpark:IdleDummy{
	default{
		+forcexybillboard +rollsprite +rollcenter
		renderstyle "add";
	}
	override void postbeginplay(){
		super.postbeginplay();
		scale*=frandom(0.2,0.5);
		roll=frandom(0,360);
	}
	states{
	spawn:
		TFOG ABCDEFGHIJ 3 bright A_FadeOut(0.08);
		stop;
	}
}

//dummy item when you don't want anything coming out for blood or puffs
class NullPuff:Actor{
	default{+nointeraction}
	states{spawn:TNT1 A 0;stop;}
}
