// ------------------------------------------------------------
// Nice movement your objects have there.
// Shame if something happened to them.
// ------------------------------------------------------------

//All monsters should inherit from this.
class HDMobBase : HDActor{
	int hdmobflags;
	flagdef doesntbleed:hdmobflags,0;
	flagdef hasdropped:hdmobflags,1;
	flagdef gibbed:hdmobflags,2;
	flagdef novitalshots:hdmobflags,3;
	flagdef hashelmet:hdmobflags,4;
	flagdef smallhead:hdmobflags,5;
	flagdef biped:hdmobflags,6;
	flagdef noshootablecorpse:hdmobflags,7;
	flagdef playingid:hdmobflags,8;
	flagdef dontdrop:hdmobflags,9;  //this is for incap dropping not item droping
	flagdef norandomweakspots:hdmobflags,10;
	flagdef noincap:hdmobflags,11;
	flagdef noblurgaze:hdmobflags,12;
	flagdef nodeathdrop:hdmobflags,13;  //skip deathdrop() check
	flagdef chasealert:hdmobflags,14;  //will call A_ShoutAlert - see HDMobster

	default{
		monster;
		radius 12;
		gibhealth 100;
		+dontgib
		-noblockmonst  //set true in HDActor, set false again in some monsters explicitly
		height 52;
		deathheight 24;
		burnheight 24;
		bloodtype "HDMasterBlood";
		hdmobbase.shields 0;
		hdmobbase.downedframe 11; //"K"
	}

	double liveheight;
	double deadheight;
	override void postbeginplay(){
		liveheight=default.height;
		deadheight=default.deathheight;
		hitboxscale=1.;
		voicepitch=1.;
		super.postbeginplay();
		resetdamagecounters();bloodloss=0;
		bplayingid=(Wads.CheckNumForName("id",0)!=-1);
	}

	override void Tick(){
		super.tick();
		if(!self||isfrozen())return;
		DamageTicker();
	}

	//randomize size
	double hitboxscale;
	void resize(double minscl=0.9,double maxscl=1.,int minhealth=0){
		double drad=radius;
		double dheight=height;
		double minchkscl=max(1.,minscl+0.1);
		double scl;
		do{
			scl=frandom(minscl,maxscl);
			A_SetSize(drad*scl,dheight*scl);
			maxscl=scl; //if this has to check again, don't go so high next time
		}while(
			//keep it smaller than the geometry
			scl>minchkscl
			&&!checkmove(pos.xy,PCM_NOACTORS)
		);
		A_SetHealth(int(health*max(scl,1)));
		scale*=scl;
		mass=int(scl*mass);
		speed*=scl;
		meleerange*=scl;

		//save a few things for future reference
		hitboxscale=scl;
		liveheight=height;
		deadheight=deathheight*scl;
	}
	override double getdeathheight(){
		return super.getdeathheight()*hitboxscale;
	}


	//give each monster a unique voice pitch
	double voicepitch;
	void A_Vocalize(sound soundname,int flags=0){
		A_StartSound(soundname,CHAN_VOICE,flags,pitch:hd_monstervoicepitch?voicepitch:1.);
	}


	//use this look function to implement voice pitch and other stuff
	virtual void OnAlert(){}
	void A_HDLook(){
		A_Look();
		if(
			instatesequence(curstate,resolvestate("see"))
			||instatesequence(curstate,resolvestate("melee"))
		){
			if(hd_monstervoicepitch) A_SoundPitch(CHAN_VOICE,voicepitch);
			OnAlert();
		}
	}


	enum ShoutAlertFlags{
		SAF_SIGHTONLY=1,  //only if target in sight
		SAF_NOMONSTERCOUNT=2,  //don't skip if too many monsters
		SAF_SILENT=4,  //don't play any sound

		SAC_MAXMONSTERCOUNT=1024,  //consider a server cvar
	}
	int nextshouttime;
	bool A_ShoutAlert(
		double chance=1.,
		int flags=0
	){
		if(
			!!target
			&&(
				!(flags&SAF_NOMONSTERCOUNT)
				||level.total_monsters<SAC_MAXMONSTERCOUNT
			)&&(
				chance>=1.
				||frandom(0,1.)<chance
			)&&(
				!(flags&SAF_SIGHTONLY)
				||checksight(target)
			)&&(
				(flags&SAF_SILENT)
				||level.time>nextshouttime
			)
		){
			nextshouttime=level.time+TICRATE;
			if(!(flags&SAF_SILENT))A_StartSound(
				random(0,2)?seesound
				:random(0,3)?activesound
				:painsound
			,CHAN_VOICE);

			//one last condition: 
			let sac=ShoutAlertCounter.Get(target);
			if(!sac){
				sac=new("ShoutAlertCounter");
				sac.target=target;
				sac.cooldown=random(TICRATE*5,TICRATE*30);
				SoundAlert(target,false,0);  //I'm assuming maxdist 0 skips all distance checks
			}
			return true;
		}
		return false;
	}
}


class ShoutAlertCounter:Thinker{
	int cooldown;
	actor target;
	override void Tick(){
		cooldown--;
		if(cooldown<1)destroy();
	}
	static ShoutAlertCounter Get(actor target){
		ThinkerIterator it=ThinkerIterator.Create("ShoutAlertCounter",STAT_STATIC);
		ShoutAlertCounter p;
		while(p=ShoutAlertCounter(it.Next())){
			if(p.target==target)return p;
		}
		return null;
	}
}


//Humanoid template
class HDHumanoid:HDMobBase{
	default{
		gibhealth 140;
		health 100;
		height 54;
		radius 12;
		deathheight 12;
		mass 120;
		speed 10;
		+hdmobbase.smallhead
		+hdmobbase.biped
		+hdmobbase.chasealert
		hdmobbase.downedframe 11;
		tag "zombie";
	}
	override void postbeginplay(){
		super.postbeginplay();
		resize(0.9,1.1);
		voicepitch=frandom(0.9,1.2);
	}
	//give armour
	hdarmourworn givearmour(double chance=1.,double megachance=0.,double minimum=0.){
		a_takeinventory("hdarmourworn");
		if(frandom(0.,1.)>chance)return null;
		let arw=hdarmourworn(giveinventorytype("hdarmourworn"));
		int maxdurability;
			if(frandom(0.,1.)<megachance){
			arw.mega=true;
			maxdurability=HDCONST_BATTLEARMOUR;
		}else maxdurability=HDCONST_GARRISONARMOUR;
		arw.durability=int(max(1,frandom(min(1.,minimum),1.)*maxdurability));
		return arw;
	}
	states{
	falldown:
		#### H 5;
		#### I 5 A_Vocalize(deathsound);
		#### JJKKK 2 A_SetSize(-1,max(deathheight,height-10));
		#### L 0 A_SetSize(-1,deathheight);
		#### L 10 A_KnockedDown();
		wait;
	standup:
		#### K 6;
		#### J 0 A_Jump(160,2);
		#### J 0 A_Vocalize(seesound);
		#### JI 4 A_Recoil(-0.3);
		#### HE 6;
		#### A 0 A_Jump(256,"see");
	}
}
class HDHoid:HDHumanoid{
	default{+nopain +nodamage}
	states{
	spawn:
	pain:
		PLAY A -1;
		stop;
	}
}
//compat wrapper for the old name
class HDMobMan:HDHumanoid{}

